FROM golang:1.16-alpine3.13 AS build-env

RUN mkdir /build
WORKDIR /build
COPY ./fio_exporter/go.mod .
COPY ./fio_exporter/go.sum .

RUN go mod download

COPY ./fio_exporter .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go/bin/fio_exporter

FROM alpine:3.13

RUN apk add --update-cache \
    fio \
  && rm -rf /var/cache/apk/*

COPY --from=build-env /go/bin/fio_exporter /go/bin/fio_exporter
ENTRYPOINT ["/go/bin/fio_exporter"]