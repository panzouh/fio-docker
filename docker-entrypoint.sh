#!/bin/sh

main () {
	if [ -f "/fio/config.ini" ]; then
		/go/bin/fio_exporter -p /fio/write -j /fio/config.ini -i 30 -l 8080
	else
		echo "$(date +"%T") | No job file found"
		exit 1
	fi
}

main
