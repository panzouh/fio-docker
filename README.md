# Fio Docker Exporter

The purpose of this helm release is to expose Fio metrics to prometheus.

Feel free to use it !
### Docker 
Docker repository : [Repository](https://hub.docker.com/repository/docker/panzouh/fio)

![Docker Image Size (tag)](https://img.shields.io/docker/image-size/panzouh/fio/0.1.0)
![Docker Image Version (latest by date)](https://img.shields.io/docker/v/panzouh/fio)

## Installation
 Edit values.yaml 

```
helm install -f values.yaml -n my-namespace ./CHART_PATH
```

## Todo
- Run tests
- Write README on Docker repository
- Add README for helm chart with configuration table

## License
License File : [License](LICENSE)

## Contribute
Comming soon !

## Credits
Kckecheng : [github/kckecheng](https://github.com/kckecheng/fio_exporter)

## Authors
@Kesslerdev
@Amassinissa
@Panzouh
